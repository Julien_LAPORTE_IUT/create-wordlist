# Password Generator Project

## Table of Contents
- [Description](#description)
- [Installation](#installation)
- [Requirements](#requirements)
- [Rust](#rust)
  - [Description](#rust-description)
  - [Usage](#rust-usage)
  - [Example](#rust-example)
- [Python](#python)
  - [Description](#python-description)
  - [Usage](#python-usage)
  - [Example](#python-example)
- [Java](#java)
  - [Description](#java-description)
  - [Usage](#java-usage)
  - [Example](#java-example)
- [C](#c)
  - [Description](#c-description)
  - [Usage](#c-usage)
  - [Example](#c-example)
- [Conclusion](#conclusion)

## Description
This project focuses on generating wordlists for passwords of varying lengths using different programming languages. The languages included are Rust, Python, Java, and C. The generated wordlists consist of combinations of uppercase and lowercase letters, numbers, and special characters.

## Installation
```bash
git clone https://gitlab.com/Julien_LAPORTE_IUT/create-wordlist.git
```

```bash
cd create-wordlist
```

## Requirements
- Python
```bash
sudo apt install python3
```

- Java
```bash
sudo apt-get install openjdk-17-jdk
```

- C
```bash
sudo apt-get install build-essential
```

- Rust
```bash
curl --proto '=https' --tlsv1.2 -sSf https://sh.rustup.rs | sh
```

## Rust
### Description
The Rust implementation utilizes the itertools library to generate permutations of characters. Wordlists are created for lengths ranging from 1 to the specified length.

### Usage
1. Run the Rust program in your terminal.
```bash
cargo run main.rs
```

2. Enter the desired wordlist length when prompted.
3. Wordlists will be generated and stored in the `wordlist_output` directory.

### Example
```bash
Wordlist length: 4
Wordlist for length 1 successfully generated
Wordlist generation time: 0 milliseconds
----------------------------------------
Wordlist for length 2 successfully generated
Wordlist generation time: 4 milliseconds
----------------------------------------
Wordlist for length 3 successfully generated
Wordlist generation time: 337 milliseconds
----------------------------------------
Wordlist for length 4 successfully generated
Wordlist generation time: 33553 milliseconds
----------------------------------------
```

## Python
### Description
The Python implementation uses the itertools module to generate product combinations. Wordlists are created for lengths ranging from 1 to the specified length.

### Usage
1. Run the Python script in your terminal.
```bash
python create_wordlist_python.py
```

2. Enter the desired wordlist length when prompted.
3. Wordlists will be generated and stored in the `wordlist_output` directory.

### Example
```bash
Wordlist length: 4
Wordlist for length 1 successfully generated in the file 'wordlist_output/wordlist_python_length_4.txt'
Wordlist generation time: 0 milliseconds
----------------------------------------
Wordlist for length 2 successfully generated in the file 'wordlist_output/wordlist_python_length_4.txt'
Wordlist generation time: 1 milliseconds
----------------------------------------
Wordlist for length 3 successfully generated in the file 'wordlist_output/wordlist_python_length_4.txt'
Wordlist generation time: 80 milliseconds
----------------------------------------
Wordlist for length 4 successfully generated in the file 'wordlist_output/wordlist_python_length_4.txt'
Wordlist generation time: 6825 milliseconds
----------------------------------------
```

## Java
### Description
The Java implementation recursively generates wordlists by exploring all possible character combinations for each length.

### Usage
1. Compile and run the Java program in your terminal.
```bash
javac create_wordlist_java.java
```

```bash
java create_wordlist_java
```

2. Enter the desired wordlist length when prompted.
3. Wordlists will be generated and stored in the `wordlist_output` directory.

### Example
```bash
Wordlist length: 4
Wordlist for length 1 successfully generated in the file 'wordlist_output/wordlist_java_length_4.txt'
Wordlist generation time: 8 milliseconds
----------------------------------------
Wordlist for length 2 successfully generated in the file 'wordlist_output/wordlist_java_length_4.txt'
Wordlist generation time: 202 milliseconds
----------------------------------------
Wordlist for length 3 successfully generated in the file 'wordlist_output/wordlist_java_length_4.txt'
Wordlist generation time: 12193 milliseconds
----------------------------------------
Wordlist for length 4 successfully generated in the file 'wordlist_output/wordlist_java_length_4.txt'
Wordlist generation time: 1250544 milliseconds
----------------------------------------
```

## C
### Description
The C implementation uses recursion to generate wordlists with all possible character combinations for each length.

### Usage
1. Compile and run the C program in your terminal.

```bash
gcc create_wordlist_c.c -o create_wordlist
```

```bash
./create_wordlist
```

2. Enter the desired wordlist length when prompted.
3. Wordlists will be generated and stored in the `wordlist_output` directory.

### Example
```bash
Wordlist length: 4
Wordlist for length 1 successfully generated in the file '../../wordlist_output/wordlist_c_length_4.txt'
Wordlist generation time: 0.869 milliseconds
----------------------------------------
Wordlist for length 2 successfully generated in the file '../../wordlist_output/wordlist_c_length_4.txt'
Wordlist generation time: 72.020 milliseconds
----------------------------------------
Wordlist for length 3 successfully generated in the file '../../wordlist_output/wordlist_c_length_4.txt'
Wordlist generation time: 6818.911 milliseconds
----------------------------------------
Wordlist for length 4 successfully generated in the file '../../wordlist_output/wordlist_c_length_4.txt'
Wordlist generation time: 684967.167 milliseconds
----------------------------------------
```

## Conclusion
Each implementation successfully generates wordlists of varying lengths, providing flexibility for different password requirements. The choice of programming language and implementation influence the efficiency of wordlist generation.

- Order of the best languages : Python > Rust > Java > C
- Screenshots of the Python program for a length 4:

As you can see in the graph below, the wordlist generation speed (words/second) increases sharply as the wordlist length increases from 1.0 to 2.0. It reaches its peak at a wordlist length of 2.0 and then declines steadily as the wordlist length continues to increase.

![Figure_Python1](screenshots/Figure_Python1.png)

As you can see in the graph, the time it takes to generate a wordlist increases exponentially as the length of the wordlist increases.

![Figure_Python2](screenshots/Figure_Python2.png)


**Note:** Screenshots are provided for illustrative purposes and may vary based on your system and execution environment.
