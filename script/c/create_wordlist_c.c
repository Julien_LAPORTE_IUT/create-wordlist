#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <time.h>

void generateWordlist(char* currentCombination, const char* allCharacters, int position, int length, const char* outputFilePath) {
    if (position == length) {
        // Writing the wordlist to the file
        FILE* file = fopen(outputFilePath, "a");
        if (file != NULL) {
            fprintf(file, "%s\n", currentCombination);
            fclose(file);
        } else {
            perror("Error opening the file");
            exit(EXIT_FAILURE);
        }
        return;
    }

    for (int i = 0; i < strlen(allCharacters); i++) {
        currentCombination[position] = allCharacters[i];
        generateWordlist(currentCombination, allCharacters, position + 1, length, outputFilePath);
    }
}

int main() {
    // Requesting the length of the wordlist and checking the value
    int wordlistLength;
    printf("Wordlist length: ");
    scanf("%d", &wordlistLength);

    if (wordlistLength < 1) {
        fprintf(stderr, "Wordlist length must be greater than 0\n");
        exit(EXIT_FAILURE);
    }

    // Creating a list of all possible characters
    const char* allCharacters = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789!@#$%^&*()-_=+[]{}|;:'\",.<>/?~";

    // Creating the output folder
    const char* outputFolder = "../../wordlist_output";
    if (mkdir(outputFolder, 0777) == -1) {
        // Ignore the error if the folder already exists
        perror("Error creating the output folder");
    }

    char outputFilePath[100];
    sprintf(outputFilePath, "%s/wordlist_c_length_%d.txt", outputFolder, wordlistLength);

    // Generate and write wordlists for all lengths from 1 to wordlistLength
    for (int length = 1; length <= wordlistLength; length++) {
        // Generate combinations for the current length
        char* currentCombination = malloc((length + 1) * sizeof(char));
        memset(currentCombination, allCharacters[0], length);
        currentCombination[length] = '\0';

        // Generate the wordlist
        clock_t timeStart = clock();
        generateWordlist(currentCombination, allCharacters, 0, length, outputFilePath);
        clock_t timeEnd = clock();

        free(currentCombination);

        // Display information for the current length
        printf("Wordlist for length %d successfully generated in the file '%s'\n", length, outputFilePath);
        printf("Wordlist generation time: %.3f milliseconds\n", ((double)(timeEnd - timeStart)) * 1000 / CLOCKS_PER_SEC);
        printf("----------------------------------------\n");
    }

    return 0;
}