import itertools
import string
import os
import time

# Requesting the length of the wordlist and checking the value
wordlist_length = int(input("Wordlist length: "))
if wordlist_length < 1:
    raise ValueError("Wordlist length must be greater than 0")

# Creating a list of all possible characters
all_characters = string.ascii_letters + string.digits + string.punctuation

# Creating the output folder
output_folder = 'wordlist_output'
os.makedirs(output_folder, exist_ok=True)
output_path = os.path.join(output_folder, 'wordlist_python_length_' + str(wordlist_length) + '.txt')

# Generate and write wordlists for all lengths from 1 to wordlist_length
for length in range(1, wordlist_length + 1):
    # Generate combinations for the current length
    wordlist_combinations = itertools.product(all_characters, repeat=length)

    # Generate the wordlist
    time_start = time.time()
    wordlist = [''.join(combination) for combination in wordlist_combinations]
    time_end = time.time()

    # Writing the wordlist to the file
    time_start2 = time.time()
    with open(output_path, 'a') as file:  # Use 'a' to append to the existing file
        file.write('\n'.join(wordlist) + '\n')
    time_end2 = time.time()

    # Display information for the current length
    print(f"Wordlist for length {length} successfully generated in the file '{output_path}'")
    print(f"Wordlist generation time: {(time_end - time_start) * 1000:.0f} milliseconds")
    print("----------------------------------------")