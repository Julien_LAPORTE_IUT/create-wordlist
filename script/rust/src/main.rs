use itertools::Itertools;
use std::fs::File;
use std::io::{self, Write};
use std::time::Instant;

fn main() {
    // Requesting the length of the wordlist and checking the value
    let mut wordlist_length_str = String::new();
    print!("Wordlist length: ");
    io::stdout().flush().unwrap();
    io::stdin().read_line(&mut wordlist_length_str).unwrap();
    let wordlist_length: usize = wordlist_length_str.trim().parse().unwrap();
    if wordlist_length < 1 {
        panic!("Wordlist length must be greater than 0");
    }

    // Creating a list of all possible characters
    let all_characters = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789!@#$%^&*()-_=+[]{}|;:'\",.<>/?~";

    // Creating the output folder
    let output_folder = "../../../wordlist_output";
    std::fs::create_dir_all(output_folder).unwrap();

    // Generate and write wordlists for lengths from 1 to wordlist_length
    let output_path = format!("{}/wordlist_rust_length_{}.txt", output_folder, wordlist_length);
    let mut file = File::create(&output_path).unwrap();

    for current_length in 1..=wordlist_length {
        // Generate combinations for the current length
        let wordlist_combinations = all_characters.chars().permutations(current_length);

        // Generate the wordlist
        let time_start = Instant::now();
        let current_wordlist: Vec<String> = wordlist_combinations.map(|combination| combination.into_iter().collect()).collect();
        let time_end = Instant::now();

        // Display information for the current length
        println!("Wordlist for length {} successfully generated", current_length);
        println!("Wordlist generation time: {:.0} milliseconds", (time_end - time_start).as_secs_f64() * 1000.0);
        println!("----------------------------------------");

        // Write current combinations to the file
        writeln!(file, "{}", current_wordlist.join("\n")).unwrap();
    }
}