package script.java;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Arrays;

public class create_wordlist_java {

    public static void main(String[] args) {
        // Requesting the length of the wordlist and checking the value
        int wordlistLength = Integer.parseInt(System.console().readLine("Wordlist length: "));
        if (wordlistLength < 1) {
            throw new IllegalArgumentException("Wordlist length must be greater than 0");
        }

        // Creating a list of all possible characters
        String allCharacters = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789!@#$%^&*()-_=+[]{}|;:'\\\",.<>/?~";

        // Creating the output folder
        String outputFolder = "wordlist_output";
        new java.io.File(outputFolder).mkdirs();
        String outputFilePath = outputFolder + "/wordlist_java_length_" + wordlistLength + ".txt";

        // Generate and write wordlists for all lengths from 1 to wordlistLength
        for (int length = 1; length <= wordlistLength; length++) {
            // Generate combinations for the current length
            char[] currentCombination = new char[length];
            Arrays.fill(currentCombination, allCharacters.charAt(0));

            // Generate the wordlist
            long timeStart = System.currentTimeMillis();
            generateWordlist(currentCombination, allCharacters, 0, length, outputFilePath);
            long timeEnd = System.currentTimeMillis();

            // Display information for the current length
            System.out.println("Wordlist for length " + length + " successfully generated in the file '" + outputFilePath + "'");
            System.out.println("Wordlist generation time: " + (timeEnd - timeStart) + " milliseconds");
            System.out.println("----------------------------------------");
        }
    }

    private static void generateWordlist(char[] currentCombination, String allCharacters, int position, int length, String outputFilePath) {
        if (position == length) {
            // Writing the wordlist to the file
            try (BufferedWriter writer = new BufferedWriter(new FileWriter(outputFilePath, true))) {
                writer.write(new String(currentCombination) + "\n");
            } catch (IOException e) {
                e.printStackTrace();
            }
            return;
        }

        for (int i = 0; i < allCharacters.length(); i++) {
            currentCombination[position] = allCharacters.charAt(i);
            generateWordlist(currentCombination, allCharacters, position + 1, length, outputFilePath);
        }
    }
}